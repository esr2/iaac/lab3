def test_nginx(host):
    nginx = host.socket('tcp://0.0.0.0:80')
    assert nginx.is_listening
